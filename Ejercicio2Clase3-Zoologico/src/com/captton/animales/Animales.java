package com.captton.animales;

public abstract class Animales {

		protected String nombre;
		protected float edad;
		protected float peso;
		
		public Animales(String nombre, float edad, float peso) {
			super();
			this.nombre = nombre;
			this.edad = edad;
			this.peso = peso;
		}
		
		public void rejuvenecer() {
			this.edad = ((this.edad*90)/100);
		}
		
		public abstract void Comer(int cant);
		
		
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public float getEdad() {
			return edad;
		}
		public void setEdad(float edad) {
			this.edad = edad;
		}
		public float getPeso() {
			return peso;
		}
		public void setPeso(float peso) {
			this.peso = peso;
		}
		
		
		
		
}
