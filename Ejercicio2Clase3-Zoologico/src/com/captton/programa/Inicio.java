package com.captton.programa;

import com.captton.animales.*;

public class Inicio {

	public static void main(String[] args) {
		
		Pajaros pajaro1 = new Pajaros("Pajarin", 4, 1200);
		Perros perro1 = new Perros("Tommy", 13, 13000);
		Peces pez1 = new Peces("Pecesin", 10, 2900);
		
		pajaro1.Comer(20);
		System.out.println(pajaro1.getPeso());
		perro1.Comer(20);
		System.out.println(perro1.getPeso());
		pez1.Comer(20);
		System.out.println(pez1.getPeso());
		
		System.out.println(pez1.getEdad());
		
		pajaro1.volar();
		pez1.nadar();
		perro1.ladrar();
		
		System.out.println(pez1.getEdad());
	
		
		

	}
	
	

}
